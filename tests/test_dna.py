import json
import pytest

from app import create_app
from app import db

from app.models import Dna


dna_mutation =  ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
dna_not_mutation =  ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"]
dna_invalid_re =  ["ATGCGA", "CAGTGS", "TTATGT", "AGAAGV", "CCCCTA", "TCACTG"]
adn_invalid_size =  ["ATGCGAA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTAC", "TCACTG"]

message_valid = 'El ADN tiene mutación'
message_invalid = 'El ADN no tiene mutación'

@pytest.fixture
def client():
    app = create_app('testing')
    with app.test_client() as client:
        with app.app_context():
            db.create_all()
            yield client
            db.session.remove()
            db.drop_all()


def test_post_200_check_dna_with_mutatio(client):
    data = {
        'dna': dna_mutation
    }
    headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
    response = client.post(
        '/mutation', data=json.dumps(data), headers=headers)

    assert response.status_code == 200
    assert response.get_json()['message'] == message_valid


def test_post_403_check_dna_without_mutation(client):
    data = {
        'dna': dna_not_mutation
    }
    headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
    response = client.post(
        '/mutation', data=json.dumps(data), headers=headers)
        
    assert response.status_code == 403
    assert response.get_json()['message'] == message_invalid


def test_post_403_check_dna_invalid_string(client):
    data = {
        'dna': dna_invalid_re
    }
    headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
    response = client.post(
        '/mutation', data=json.dumps(data), headers=headers)
        
    assert response.status_code == 422


def test_post_403_check_dna_invalid_size(client):
    data = {
        'dna': adn_invalid_size
    }
    headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
    response = client.post(
        '/mutation', data=json.dumps(data), headers=headers)
        
    assert response.status_code == 422


def test_get_200_data_dna(client):
    headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
    response = client.get(
        '/stats', headers=headers)

    print(response.get_json())
        
    assert response.status_code == 200