from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from instance.config import app_config


db = SQLAlchemy()
migrate = Migrate()
from .models import *  # noqa
from .api import api  # noqa


def create_app(config_name):
    """
    Factory: setup and returns a Flask App given a configuration
    name 'config_name' ['development'|'testing'|'staging']
    """

    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.register_blueprint(api)
    db.init_app(app)
    migrate.init_app(app, db)

    return app