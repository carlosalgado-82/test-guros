from app import db


class Dna(db.Model):
    __tablename__ = 'dna'

    id = db.Column(db.Integer, primary_key=True)
    dna = db.Column(db.Text, nullable=False)
    dna_hash = db.Column(db.String(100), nullable=False)
    has_mutations = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return '<DNA %r>' % self.id