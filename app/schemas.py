import hashlib

from marshmallow import (Schema, ValidationError, fields, 
                        validate, validates_schema, post_load)


class DnaSchema(Schema):
    dna = fields.List(fields.String(
        validate=validate.Regexp('^[ATCG]+$')
    ), required=True)

    @validates_schema
    def validate_dna(self, data, **kwargs):
        dna_size = len(data['dna'])
        for row in data['dna']:
            if dna_size != len(row):
                raise ValidationError("Size of one row it's incorrect")

    @post_load
    def add_dna_data(self, data, **kwargs):
        dna = data['dna']
        dna_str = ','.join(dna)
        data['dna_str'] = dna_str
        data['dna_hash'] = hashlib.md5(dna_str.encode('utf-8')).hexdigest()

        return data