def hasMutation(dna):
    num_mutation = 0
    min_mutation = 1
    min_size = 4
    dna_size = len(dna[0])
    max_check = dna_size - (min_size - 1)

    # Check Vertical 
    for i in range(max_check):
        for j in range(dna_size):
            aux_dna = dna[i][j]
            if(
                dna[i + 1][j] == aux_dna and 
                dna[i + 2][j] == aux_dna and 
                dna[i + 3][j] == aux_dna
            ):
                num_mutation = num_mutation + 1
                if num_mutation > min_mutation:
                    return True

    # Check Horizontal 
    for i in range(dna_size):
        for j in range(max_check):
            aux_dna = dna[i][j]
            if(
                dna[i][j + 1] == aux_dna and 
                dna[i][j + 2] == aux_dna and 
                dna[i][j + 3] == aux_dna
            ):
                num_mutation = num_mutation + 1
                if num_mutation > min_mutation:
                    return True

    # Check Diagonal Desc (\)
    for i in range(max_check):
        for j in range(max_check):
            aux_dna = dna[i][j]
            if(
                dna[i + 1][j + 1] == aux_dna and 
                dna[i + 2][j + 2] == aux_dna and 
                dna[i + 3][j + 3] == aux_dna
            ):
                num_mutation = num_mutation + 1
                if num_mutation > min_mutation:
                    return True

    # Check Diagonal Asc (/) 
    for i in range(max_check, dna_size):
        for j in range(0, max_check):
            aux_dna = dna[i][j]
            if(
                dna[i - 1][j + 1] == aux_dna and 
                dna[i - 2][j + 2] == aux_dna and 
                dna[i - 3][j + 3] == aux_dna
            ):
                num_mutation = num_mutation + 1
                if num_mutation > min_mutation:
                    return True
    
    return False