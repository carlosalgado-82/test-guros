from flask import request
from marshmallow import ValidationError

from . import api
from app import db

from app.models import Dna
from app.schemas import DnaSchema
from app.helpers import hasMutation


@api.route('/mutation', methods=['POST'])
def check_mutation():
    schema = DnaSchema()
    json_data = request.get_json()

    if not json_data:
        return {'error': 'No input data provided'}, 400
    try:
        data = schema.load(json_data)
    except ValidationError as err:
        return err.messages, 422

    dna = Dna.query.filter_by(dna_hash=data['dna_hash']).first()

    if dna:
        has_mutation = dna.has_mutations
    else:
        has_mutation = hasMutation(data['dna'])
        dna = Dna(
            dna=data['dna_str'],
            dna_hash=data['dna_hash'],
            has_mutations=has_mutation
        )
        db.session.add(dna)
        db.session.commit()

    code = 200 if has_mutation else 403
    message = 'El ADN tiene mutación' if has_mutation \
            else 'El ADN no tiene mutación'

    return {'message': message}, code


@api.route('/stats', methods=['GET'])
def get_stats():
    not_mutations = Dna.query.filter(Dna.has_mutations == False).count()
    has_mutations = Dna.query.filter(Dna.has_mutations == True).count()
    ratio = (has_mutations/not_mutations) if not_mutations > 0 else 0.0
    data = {
        "count_mutations": has_mutations,
        "count_no_mutations": not_mutations,
        "ratio": ratio
    }

    return data, 200