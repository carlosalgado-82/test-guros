import os


class Config(object):
    """
    Default configuration.
    """

    DEBUG = False
    FLASK_SECRET = os.getenv('FLASK_SECRET')
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL')
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(Config):
    """
    Development configuration.
    """

    TESTING = True
    DEBUG = True


class TestingConfig(Config):
    """
    Testing configuration.
    """

    TESTING = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.getenv('TEST_DATABASE_URL')


class StagingConfig(Config):
    """
    Staging configuration.
    """

    DEBUG = True


app_config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'staging': StagingConfig
}