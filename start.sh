#!/bin/bash

# Create virtual env if not exist.
if [ ! -d "./venv" ]; then
  echo "Creating virtual env..."
  python3 -m venv venv
fi

echo "env activation"
source venv/bin/activate

echo "Installing requirements..."
pip install -r requirements.txt

echo "Migration..."
flask db upgrade

echo "Starting on ${FLASK_RUN_HOST}:${FLASK_RUN_PORT}..."
flask run