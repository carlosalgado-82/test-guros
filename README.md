# DNA API

API para evaluar cadenas de ADN y conocer si cuentan con mutaciones, así como obtener una estadistica de los ADN evaluados y cuantos cuentan con mutaciones y cuantos no.

## Ejecutar la App

Para ejecutar la App se puede realizar de dos formas, por medio de un servidor virtual o por medio de docker, a continuación explico ambas opciones:

#### Servidor virtual

Para poder ejecutar la App de este modo es necesario contar con los siguientes requisitos:

* Tener instalado python3.
* Tener instalado postgresql.
* Acceder a postgres y crear la BD `adn_db`.

Una vez que tengamos los requisitos previos, ejecutamos lo siguiente para iniciar la app:

* Crear una copia del archivo `.env.example.virtual` con el nombre de `.env` y reemplazar los valores de $USER, $PASSWORD y $HOST del acceso de tu postgres en la variable ``DATABASE_URL``
* Crear un entorno de ambiente ``python3 -m venv venv``
* Activar el entorno ``source venv/bin/activate``
* Instalar las dependencias ``pip install -r requirements.txt``
* Iniciar el proyecto ``flask run``

#### Docker

Para poder ejecutar la con Docker debemos contar con Docker instalado y seguir los siguientes comandos:

* Crear una copia del archivo `.env.example` con el nombre de `.env`
* Iniciar el servidor ``docker-compose up``

## API 

La App cuenta con 2 peticiones para ejecutar las diferentes opciones.

POST ```/mutation```
Este endpoint se le envía cadenas de ADN para la verificación de posible mutaciones que pueda tener, en caso de tener mutaciones regresa un código `200` y en caso contrario un `403`.
```
{
    "dna":  ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"]
}
```

GET ```/stats```
Este endpoint al solicitarlo regresa una estadistica de los ADN evaluados previamente por medio del endpoint de POST ```/mutation```, regresa el número de ADN con mutaciones, sin mutaciones y el ratio entre ambos valores.

## CASOS DE PRUEBA

Para ejecutar los diferentes escenarios se pueden utilizar los siguientes valores:

`Secuencia con mutaciones`.
```sh
adn =  ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
```

`Secuencia sin mutaciones`.
```sh
adn =  ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"]
```

`Secuencia con carácteres invalidos`.
```sh
adn =  ["ATGCGA", "CAGTGS", "TTATGT", "AGAAGV", "CCCCTA", "TCACTG"]
```

`Secuencia con un tamaño invalidos`.
```sh
adn =  ["ATGCGAA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTAC", "TCACTG"]
```

## TESTS

La app cuenta integrado tests unitarios los cuales también se encuentran integrados con CI/CD dentro del repositorio y estos se ejecutan de manera automatica cada que se realiza un commit al proyecto.

Adicional se evaluaron los test y cubren un 91% del proyecto:

![Preview](docs/test.png)