import os
from app import create_app


CONFIG_NAME = os.getenv('FLASK_ENV')

app = create_app(CONFIG_NAME)


@app.route('/')
def index():
    return 'Hello world!'