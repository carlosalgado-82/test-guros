FROM python:3.9

RUN apt update

WORKDIR /code

COPY requirements.txt /code/

RUN pip install -r requirements.txt